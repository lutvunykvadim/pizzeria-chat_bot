import enums.*;

import java.util.*;

public class Dialog {

    public static void hello() {
        System.out.println("Hello, choose from the following list of orders what you want:\n");
        for (Drink item : Drink.values())
            System.out.println(item.getNameOrder() + " " + item.getPriceOrder());

        for (Pizza item : Pizza.values())
            System.out.println(item.getNameOrder() + " " + item.getPriceOrder());


    }

    public static void specialOffer() {
        System.out.println(" We have a special offer \"Each third pizza is free\". You may order one more pizza free.");
    }

    public static void youOrder() {
        System.out.println("You order is:");
        Set<String> youOrder = new HashSet<>(ServisBot.youOrder);
        for (String s : youOrder) {
            System.out.println(Collections.frequency(ServisBot.youOrder, s) + " " + s);
        }
        System.out.println("Total price is: " + Count.totalPrice);
        if (ServisBot.freePizzaCount() > 0) {

            Set<String> youFreeOrder = new HashSet<>(ServisBot.freePizza);
            System.out.print("You got for free: ");
            for (String s : youFreeOrder) {
                System.out.print(Collections.frequency(ServisBot.freePizza, s) + " " + s + " ");
            }
        }
    }
}


