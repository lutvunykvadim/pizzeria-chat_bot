package enums;

public enum Pizza {
    pizza4Cheeses("pizza_4_cheeses", 80.00f),
    pizzaTropico("pizza_tropico", 90.00f),
    pizzaCaesar("pizza_caesar", 100.00f),
    pizzaMilano("pizza_milano", 100.00f),
    pizzaSpecial("pizza_special", 110.00f),
    pizzaMargarita("pizza_margarita", 120.00f);
    private final String nameOrder;
    private final float priceOrder;


    Pizza(String nameOrder, float priceOrder) {
        this.nameOrder = nameOrder;
        this.priceOrder = priceOrder;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public float getPriceOrder() {
        return priceOrder;
    }
}
