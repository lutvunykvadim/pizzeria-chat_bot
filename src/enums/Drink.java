package enums;

public enum Drink {
    redWine("red_wine", 20.00f),
    appleJuce("apple_juce", 40.00f);

    private String nameOrder;
    private float priceOrder;

    Drink(String nameOrder, float priceOrder) {
        this.nameOrder = nameOrder;
        this.priceOrder = priceOrder;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public float getPriceOrder() {
        return priceOrder;
    }
}
