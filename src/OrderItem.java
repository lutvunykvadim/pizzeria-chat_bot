import enums.ItemSubType;

public class OrderItem {

    private String name;
    private float price;
    private final ItemSubType itemSubType;

    OrderItem(String text, float value, ItemSubType type) {
        this.name = text;
        this.price = value;
        this.itemSubType = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public ItemSubType getItemSubType() {
        return itemSubType;
    }



       }
