import enums.Drink;
import enums.ItemSubType;
import enums.Pizza;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;


public class ServisBot {

    static List<OrderItem> orderItems = new ArrayList<>();
    static List<String> freePizza = new ArrayList<>();
    static List<String> youOrder = new ArrayList<>();

    void inputOrder() {
        StringBuilder anotherWords = new StringBuilder();

        do {
            anotherWords.setLength(0);
            Scanner in = new Scanner(System.in);
            String input = in.nextLine();
            String[] itemsOrders = input.split(" ");
            boolean drinkBoolean = false;
            boolean pizzaBoolean = false;

            for (String inputWord : itemsOrders) {

                for (Pizza itemPizza : Pizza.values()) {

                    if (inputWord.contentEquals(itemPizza.getNameOrder())) {
                        orderItems.add(new OrderItem(itemPizza.getNameOrder(), itemPizza.getPriceOrder(), ItemSubType.PIZZA));
                        youOrder.add(itemPizza.getNameOrder());
                        Count.countPizza++;
                        pizzaBoolean = true;

                    }

                }

                for (Drink itemDrink : Drink.values()) {

                    if (inputWord.contentEquals(itemDrink.getNameOrder())) {
                        orderItems.add(new OrderItem(itemDrink.getNameOrder(), itemDrink.getPriceOrder(), ItemSubType.DRINK));
                        youOrder.add(itemDrink.getNameOrder());
                        drinkBoolean = true;
                    }

                }

                if (!(drinkBoolean || pizzaBoolean)) {
                    anotherWords.append(inputWord).append(" ");
                }
                drinkBoolean = false;
                pizzaBoolean = false;
            }

            if (anotherWords.length() > 0)
                System.out.println("Please check spelling and repeat " + anotherWords);
        }
        while (anotherWords.length() > 0);
        orderItems.sort(Comparator.comparing(OrderItem::getItemSubType).thenComparing(OrderItem::getPrice).reversed());

    }

    void specialOffer() {
        while ((Count.countPizza + 1) % 3 == 0) {
            Dialog.specialOffer();
            inputOrder();
        }
    }

    static int freePizzaCount() {
        return Count.countPizza / 3;
    }

    void analyseList() {

        for (int countPizza = freePizzaCount(), n = 1; countPizza > 0; countPizza--) {
            orderItems.get(orderItems.size() - n).setPrice(0);
            freePizza.add((orderItems.get(orderItems.size() - n)).getName());
            n++;
        }
    }

    void totalPrice() {
        for (OrderItem item : orderItems)
            Count.totalPrice += item.getPrice();
    }


}



